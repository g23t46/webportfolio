$(document).ready(function() {

	//In order to force the menu to collapse if the window size changes to non-smartphone.
	$(window).resize(checkSize);

	//Collapsible menu
	var menuIsCollapsed = ($(".main-menu-collapsible").css("display") == "none");
	$("li.collapse-icon").click(function(e) {
		e.preventDefault();
		$(".main-menu-collapsible").slideToggle(300);
	});
});

//Checking just the window size has still bugs in older browsers, so this hack seems better.
function checkSize(){
	//If the normal menu's position is flex, then the size is that of a smartphone
	var sizeIsSmartphone = ($(".main-menu").css("display") == "flex");

    if (!sizeIsSmartphone){
        $(".main-menu-collapsible").css("display", "none");
    }

}
